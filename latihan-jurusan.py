jurusan = str(input("Pilih Jurusan : "))
jam = str(input("Pilih Jam : "))

if jurusan == "SI":
    szjurusan = "Sistem Informasi"
    if jam == "PG":
        szjam = "07.00-16.00"
        biaya = "1.000.000"
    else:
        szjam = "16.00-21.00"
        biaya = "1.500.000"
else:
    szjurusan = "Teknik Informatika"
    if jam == "PG":
        szjam = "07.00-16.00"
        biaya = "2.000.000"
    else:
        szjam = "16.00-21.00"
        biaya = "2.500.000"

print("Anda memilih jurusan",szjurusan)
print("dengan pilihan jam",szjam)
print("dengan biaya",biaya)